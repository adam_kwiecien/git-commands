# Index/Working tree

### Add to staging area

* `git add [pattern]`
* patterns:
    * file1 file2 file3
    * . [add all]
    * *.txt
* desc: This command adds files to staging area

### Add part of file to staging area

* `git add [pattern] -p`
* patterns:
    * file1 file2 file3
    * . [add all]
    * *.txt
* desc: This command let's you choose which part of file to add to staging area

### Restore file from previous commit

* `git restore --source=HEAD~1 [file]`
* `git restore --source=[commit] [file]`
* desc: to discard changes in working directory, **this can not be reversed by git**

### Remove from working directory

* `git restore [file]`
* desc: to discard changes in working directory, this doesn't work on untracked files

### Remove untracked from working directory

* `git clean`
* desc: to discard changes in working directory, **this can not be reversed by git**

### Remove from staging area

* `git restore --staged`
* desc: This removes files from staging area

### Shows all files that are in staging area

* `git ls-files`
* desc: This merges the file listing in the index with the actual working directory list, and shows different
  combinations of the two.

### Do not follow locally

* `git update-index --assume-unchanged file`
* desc: Remove file from local index, it doesn't change file
* undo: `git update-index --no-assume-unchanged file`

### Show files in staging area

*`git status`

* desc: Displays paths that have differences between the index file and the current HEAD commit, paths that have
  differences between the working tree and the index file, and paths in the working tree that are not tracked by Git (
  and are not ignored by gitignore[5]). The first are what you would commit by running git commit; the second and third
  are what you could commit by running git add before running git commit.
  *`git checkout [branch name]`

### Ignore file mod

* `git config --global core.filemode false`
* for newer versions use
    * `git config --add --global core.filemode false`

# Repository

### Clone repository

* `git clone [repository url]`
* desc: Clones a repository into a newly created directory, creates remote-tracking branches for each branch in the
  cloned repository (visible using git branch --remotes), and creates and checks out an initial branch that is forked
  from the cloned repository’s currently active branch.

### Change branch

* `git checkout [branch name]`
* desc: Move to other branch, error may occur when having uncommitted changes

### Switch to new branch

* `git checkout -b [branch name]`
* desc: Specifying -b causes a new branch to be created as if git-branch[1] were called and then checked out. In this
  case you can use the --track or --no-track options, which will be passed to git branch. As a convenience, --track
  without -b implies branch creation; see the description of --track below. If -B is given, <new_branch> is created if
  it doesn't exist; otherwise, it is reset.

### Commit

* `git commit -m "[commit message]"`
* desc: Creates snapshot of the current contents of the index and the given log message describing the changes

### Amend commit

* `git commit --amend`
* desc: Changes last commit message. Useful for fixing typos in commit messages

### Fetch

* `git fetch`
* desc: Fetch branches and/or tags (collectively, "refs") from one or more other repositories, along with the objects
  necessary to complete their histories. Remote-tracking branches are updated, local files are untouched.

### Push to remote

* `git push`
* desc: Updates remote refs using local refs, while sending objects necessary to complete the given refs.

### Init

* `git init [repository name]`
* desc: This command creates an empty Git repository

### Show log

* `git log`
* desc: Shows the commit logs.

List commits that are reachable by following the parent links from the given commit(s), but exclude commits that are
reachable from the one(s) given with a ^ in front of them. The output is given in reverse chronological order by
default.

### Rename file

* `git mv file1 file2`
* desc: Changes file1 name to file2, git logs this as file name change not a deletion and creation

### Stash

* `git stash`
* desc: The command saves your local modifications away and reverts the working directory to match the HEAD commit.

### list stashed changes

* `git stash list`
* docs: List all changes that has been stashed
*

### apply stashed changes

* `git stash apply`
* docs: Add stashed changes to working tree, (can be different commit/branch the one that stash has been created from)

### sync repository and .gitignoreremove
* ```git rm --cached `git ls-files -i -X .gitignore` ```
* docs: Remove all files from repository that are mentioned in .gitignore but do not remove them from working tree
* warning: If all files work fine, git will show `usage: git rm [<options>] [--] <file>... `. This is caoused by git ls-files not showing any files

### remove file only from repository
* `git rm --cached file`
* docs: Remove a file from a repository without deleting it from working tree


# Settings

Config file location

* --global use global config file
* --system use system config file)
* --local use repository config file
* --worktree use per-worktree config file
* -f, --file <file>     use given config file
* --blob <blob-id>      read config from given blob object


* desc `git config --global [option] "[value]"`
    * e.g. options:
        * user.name
        * user.email
        * core.editor

### Edit all global settings

`git config --global -e`

### End of line config

* Windows: `git config --global core.autocrlf true`
* Unix: `git config --global core.autocrlf input`

# Good Practise

### Basic rules

* Do not commit every change and do not commit once per feature, commit every milestone in feature
    * rule of thumb is to commit every time new test passes
* Use present tense in commit message e.g.
    * ❌ fixed a bug
    * ✅ fix a bug

### Conventional Commits

1. #### Commits

**option(scope): short message \n \n long message if needed**

* feat(): [message]
* fix(): [message]
* docs(): [message]
* chore(): [message]
* refactor(): [message]
* tests(): [message]
* styles(): [message]
* ci(): [message]
* build(): [message]
* revert(): [message]

#### 2. Branches

**option/short-description**

* feature/[short-description]
* fix/[short-description]
* docs/[short-description]
* chore/[short-description]
* refactor/[short-description]
* tests/[short-description]
* styles/[short-description]
* ci/[short-description]
* build/[short-description]
* revert/[short-description]

# Interesting:

### advanced commit

* `git commit -am "[message]"`
* `git add . git commit -m "[message]"`
* desc: ^ same thing, use this only when sure it's ok

### selecting commits

* HEAD~[x]
* desc: select x commit back
* e.g.
    * HEAD~1 -> select last commit
    * HEAD~2 -> select penultimate commit

### Check git version

* `git --version`
* desc: Shows installed git version can have file in staging area and in

# Naming

### Areas:

* Working directory: Local files
* Index: between the working directory and repository, allows you to review and check what wil be saved in repository
* Staging area: Same as Index
* Repository: Collection of snapshots of working tree
* remote Repository: Remote collection of snapshots of working tree

* ![git area image](https://blog.isquaredsoftware.com/images/2021-01-career-advice-git-usage/git-staging-workflow.png)
